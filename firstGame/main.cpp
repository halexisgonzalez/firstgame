#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include "GameWindow.h"
#include "OptionWindow.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    // Creamos una ventana principal
    QWidget window;
    window.setWindowTitle("Pong");
    window.resize(250, 150);

    // Creamos un layout vertical para organizar los botones
    QVBoxLayout* layout = new QVBoxLayout(&window);

    // Creamos los botones del menú
    QPushButton* startButton = new QPushButton("Iniciar juego", &window);
    QPushButton* optionsButton = new QPushButton("Opciones", &window);
    QPushButton* quitButton = new QPushButton("Salir", &window);

    // Agregamos los botones al layout
    layout->addWidget(startButton);
    layout->addWidget(optionsButton);
    layout->addWidget(quitButton);

    // Creamos la ventana del juego
    GameWindow gameWindow;

    // Conectamos las señales y slots para los botones
    QObject::connect(startButton, &QPushButton::clicked, [&gameWindow]() {
        // Mostramos la ventana del juego
        gameWindow.show();
    });

    // Creamos la venta opciones
    OptionWindow optionWindow;

    QObject::connect(optionsButton, &QPushButton::clicked, [&optionWindow](){
        // Mostramos la ventana opciones
        optionWindow.show();
    });

    QObject::connect(quitButton, &QPushButton::clicked, &app, &QApplication::quit);

    // Mostramos la ventana principal y ejecutamos la aplicación
    window.show();
    return app.exec();
}
