#include "GameWindow.h"

// Dimensiones del tablero
const int BOARD_WIDTH = 800;
const int BOARD_HEIGHT = 600;

// Dimensiones de los jugadores
const int PLAYER_WIDTH = 15;
const int PLAYER_HEIGHT = 80;

// Dimensiones de la pelota
const int BALL_SIZE = 10;

GameWindow::GameWindow(QWidget* parent) : QWidget(parent), player1Y(0), player2Y(0), ballX(0), ballY(0),
                                            ballXDir(1), ballYDir(1) {
    setWindowTitle("Juego de Pong");
    setFixedSize(BOARD_WIDTH, BOARD_HEIGHT);
    setFocusPolicy(Qt::StrongFocus);

    // Creamos un temporizador para actualizar el juego periódicamente
    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &GameWindow::updateGame);
    timer->start(10); // Actualiza el juego cada 10 milisegundos

    // Inicializamos las posiciones de los jugadores
    player1Y = (BOARD_HEIGHT - PLAYER_HEIGHT) / 2;
    player2Y = (BOARD_HEIGHT - PLAYER_HEIGHT) / 2;

    // Inicializamos la posición de la pelota
    ballX = BOARD_WIDTH / 2;
    ballY = BOARD_HEIGHT / 2;
}

void GameWindow::paintEvent(QPaintEvent* event) {
    Q_UNUSED(event);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    // Dibujamos los jugadores
    painter.fillRect(0, player1Y, PLAYER_WIDTH, PLAYER_HEIGHT, Qt::white);
    painter.fillRect(BOARD_WIDTH - PLAYER_WIDTH, player2Y, PLAYER_WIDTH, PLAYER_HEIGHT, Qt::white);

    // Dibujamos la pelota
    painter.setBrush(Qt::white);
    painter.drawEllipse(ballX - BALL_SIZE / 2, ballY - BALL_SIZE / 2, BALL_SIZE, BALL_SIZE);
}

void GameWindow::keyPressEvent(QKeyEvent* event) {
    // Movimiento del jugador 1 (teclas W y S)
    if (event->key() == Qt::Key_W) {
        player1Y -= 5;
        if (player1Y < 0)
            player1Y = 0;
    } else if (event->key() == Qt::Key_S) {
        player1Y += 5;
        if (player1Y > BOARD_HEIGHT - PLAYER_HEIGHT)
            player1Y = BOARD_HEIGHT - PLAYER_HEIGHT;
    }

    // Movimiento del jugador 2 (teclas de flechas arriba y abajo)
    if (event->key() == Qt::Key_Up) {
        player2Y -= 5;
        if (player2Y < 0)
            player2Y = 0;
    } else if (event->key() == Qt::Key_Down) {
        player2Y += 5;
        if (player2Y > BOARD_HEIGHT - PLAYER_HEIGHT)
            player2Y = BOARD_HEIGHT - PLAYER_HEIGHT;
    }

    // Solicitamos un repintado de la ventana
    update();
}

void GameWindow::updateGame() {
    // Movimiento de la pelota
    ballX += ballXDir;
    ballY += ballYDir;

    // Comprobamos colisiones con los bordes verticales
    if (ballY - BALL_SIZE / 2 < 0 || ballY + BALL_SIZE / 2 > BOARD_HEIGHT)
        ballYDir = -ballYDir;

    // Comprobamos colisiones con los jugadores
    if (ballX - BALL_SIZE / 2 < PLAYER_WIDTH && ballY > player1Y && ballY < player1Y + PLAYER_HEIGHT)
        ballXDir = -ballXDir;
    if (ballX + BALL_SIZE / 2 > BOARD_WIDTH - PLAYER_WIDTH && ballY > player2Y && ballY < player2Y + PLAYER_HEIGHT)
        ballXDir = -ballXDir;

    // Solicitamos un repintado de la ventana
    update();
}
