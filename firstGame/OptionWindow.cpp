#include <QPushButton>
#include <QVBoxLayout>
#include "OptionWindow.h"

OptionWindow::OptionWindow(QWidget *parent) : QMainWindow{parent}
{
    setWindowTitle("Opciones");
    resize(250, 150);

    // Creamos un layout vertical para organizar los botones
    QVBoxLayout* layout = new QVBoxLayout();

    // Creamos los botones del menú
    QPushButton* playersButton = new QPushButton("Players");
    QPushButton* ballButton = new QPushButton("Tamaño Bola");
    QPushButton* backButton = new QPushButton("Volver");

    // Agregamos los botones al layout
    layout->addWidget(playersButton);
    layout->addWidget(ballButton);
    layout->addWidget(backButton);

    // Creamos un widget central y establecemos el layout
    QWidget* centralWidget = new QWidget(this);
    centralWidget->setLayout(layout);
    setCentralWidget(centralWidget);

    QObject::connect(backButton, &QPushButton::clicked, this, &OptionWindow::close);
}
