#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QWidget>
#include <QKeyEvent>
#include <QTimer>
#include <QPainter>

class GameWindow : public QWidget {
public:
    GameWindow(QWidget* parent = nullptr);

protected:
    void paintEvent(QPaintEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;
    void updateGame();

private:
    int player1Y;   // Posición vertical del jugador 1
    int player2Y;   // Posición vertical del jugador 2

    int ballX;      // Posición horizontal de la pelota
    int ballY;      // Posición vertical de la pelota
    int ballXDir;   // Dirección horizontal de la pelota
    int ballYDir;   // Dirección vertical de la pelota
};

#endif // GAMEWINDOW_H
